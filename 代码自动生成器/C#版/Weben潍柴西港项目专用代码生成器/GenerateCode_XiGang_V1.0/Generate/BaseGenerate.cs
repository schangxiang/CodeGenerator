﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateCode_GEBrilliantFactory
{
    public class BaseGenerate
    {
        public static void CommonReplace(ref string str)
        {
            str = str.Replace("$ProjectNamePrefix$", MainForm.projectNamePrefix);
            str = str.Replace("$DataBaseName$", MainForm.dataBaseName);
        }
    }
}
