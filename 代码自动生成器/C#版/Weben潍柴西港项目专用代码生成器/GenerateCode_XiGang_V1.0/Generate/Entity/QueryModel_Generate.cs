﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenerateCode_GEBrilliantFactory
{
    /// <summary>
    /// 生成查询实体类
    /// </summary>
    public class QueryModel_Generate : BaseGenerate
    {
        public static string CreateQueryModelLText(string Modulelogo,
            string ChinaComment, List<ColumnModel> columnNameList, string entityName)
        {
            var str = TextHelper.ReadText(@"Templete\Entity\QueryModel模板.txt");
            CommonReplace(ref str);

            str = str.Replace("$ChinaComment$", ChinaComment);//中文注释
            str = str.Replace("$EntityName$", entityName);
           
            str = str.Replace("$Modulelogo$", Modulelogo);//模块简写

            string attrString = "";

            List<ColumnModel> newColumnNameList = ListHelper.OnlyRemoveId(columnNameList);
            for (int i = 0; i < newColumnNameList.Count; ++i)
            {
                attrString += StructStrHelper.GenerateAttributeForQueryModel(newColumnNameList[i]);
                attrString += StructStrHelper.GenerateAttributeForQueryModelMode(newColumnNameList[i]);
            }
            str = str.Replace("$QueryAttributes$", attrString);
           
            return str;
        }
    }
}
