﻿

using System.Collections.Generic;

namespace GenerateCode_GEBrilliantFactory
{
    /// <summary>
    /// 生成DAL
    /// </summary>
    public class DAL_Generate : BaseGenerate
    {
        public static string CreateDALText(string filePrefixName, string TableName, string entityName, string Author,
            string ChinaComment, string primaryKey, string primaryKeyDesc, string Modulelogo,string tableAlias,
            List<ColumnModel> columnNameList)
        {

            var str = TextHelper.ReadText(@"Templete\DAL模板.txt");

            CommonReplace(ref str);

            //存储过程名
            ProcName procName = CommonHelper.GetProcName(Modulelogo);
            str = str.Replace("$AddProcName$", procName.AddProc);
            str = str.Replace("$UpdateProcName$", procName.UpdateProc);
            str = str.Replace("$GetSingleProcName$", procName.GetSingleProc);
            str = str.Replace("$GetListProcName$", procName.ListProc);
            str = str.Replace("$GetPageListProcName$", procName.PageListProc);



            str = str.Replace("$TableName$", TableName);//表名
            str = str.Replace("$Author$", Author);//作者
            str = str.Replace("$ChinaComment$", ChinaComment);//中文注释
            str = str.Replace("$CurDate$", CommonHelper.GetCurDate());//当前时间
            str = str.Replace("$EntityName$", entityName);//实体类名
            str = str.Replace("$TableAlias$", tableAlias);//表别名

            str = str.Replace("$FilePrefixName$", filePrefixName);//模块名
            str = str.Replace("$Modulelogo$", Modulelogo);//模块简写
            str = str.Replace("$PrimaryKey$", primaryKey);//主键
            str = str.Replace("$PrimaryKeyDesc$", primaryKeyDesc);//主键描述

          

            str = str.Replace("$ToSingleModel$", StructStrHelper.GetToModelStr(columnNameList));//动态给实体类赋值 
            str = str.Replace("$AddSqlParameter$", StructStrHelper.GetParameterForAddDAL(columnNameList));
            str = str.Replace("$UpdateSqlParameter$", StructStrHelper.GetParameterForUpdateDAL(columnNameList));
            str = str.Replace("$QueryPageSqlParameter$", StructStrHelper.GetParameterForQueryPageDAL(columnNameList));
            return str;
        }
    }
}
