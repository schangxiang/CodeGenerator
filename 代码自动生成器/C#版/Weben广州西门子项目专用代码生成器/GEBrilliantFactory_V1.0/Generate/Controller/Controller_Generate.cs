﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenerateCode_GEBrilliantFactory
{
    /// <summary>
    /// 生成Controller类
    /// </summary>
    public class Controller_Generate : BaseGenerate
    {
        public static string CreateText(string Modulelogo,
            string ChinaComment, List<ColumnModel> columnNameList, string entityName, string orderByName = "")
        {
            var str = TextHelper.ReadText(@"Templete\Controller模板.txt");
            CommonReplace(ref str);
            str = str.Replace("$ChinaComment$", ChinaComment);//中文注释
            str = str.Replace("$EntityName$", entityName);
            str = str.Replace("$Modulelogo$", Modulelogo);//模块简写
            str = str.Replace("$OrderByName$", orderByName == "" ? "Id" : orderByName);//模块简写

            str = str.Replace("$ValidateEmptyForInsert$", StructStrHelper.GetValidateEmptyStrForController(columnNameList));
            str = str.Replace("$ValidateEmptyForUpdate$", StructStrHelper.GetValidateEmptyStrForController(columnNameList, false));



            var pageStr = StructStrHelper.GetPageFilterStrForController(columnNameList);
            str = str.Replace("$PageSearchFilter$", pageStr);

            return str;
        }
    }
}
