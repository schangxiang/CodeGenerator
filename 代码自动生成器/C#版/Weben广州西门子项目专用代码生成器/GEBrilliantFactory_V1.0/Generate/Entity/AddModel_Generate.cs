﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenerateCode_GEBrilliantFactory
{
    /// <summary>
    /// 生成新增参数实体类
    /// </summary>
    public class AddModel_Generate : BaseGenerate
    {
        public static string CreateAddModelLText(string addEntityParam,
            string ChinaComment, List<ColumnModel> columnNameList)
        {
            var str = TextHelper.ReadText(@"Templete\Entity\AddModel模板.txt");
            CommonReplace(ref str);
            str = str.Replace("$ChinaComment$", ChinaComment);//中文注释

            str = str.Replace("$AddEntityParam$", addEntityParam);

            string attrString = "";

            List<ColumnModel> newColumnNameList = ListHelper.RemoveIdCreatorModifier(columnNameList);
            for (int i = 0; i < newColumnNameList.Count; ++i)
            {
                attrString += StructStrHelper.GenerateAttributeForQueryModel(newColumnNameList[i]);
            }
            str = str.Replace("$AddAttributes$", attrString);

            return str;
        }
    }
}
