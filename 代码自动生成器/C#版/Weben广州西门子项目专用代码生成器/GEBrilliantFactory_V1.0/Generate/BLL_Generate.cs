﻿
using System.Collections.Generic;

namespace GenerateCode_GEBrilliantFactory
{
    /// <summary>
    /// 生成BLL
    /// </summary>
    public class BLL_Generate : BaseGenerate
    {
        public static string CreateBLLText(string filePrefixName, string TableName, string entityName, string Author,
            string ChinaComment, string primaryKey, string primaryKeyDesc, string Modulelogo, string tableAlias,
            string addEntityParam,
            List<ColumnModel> columnNameList)
        {
            var str = TextHelper.ReadText(@"Templete\BLL模板.txt");

            CommonReplace(ref str);

            str = str.Replace("$TableName$", TableName);//表名
            str = str.Replace("$Author$", Author);//作者
            str = str.Replace("$ChinaComment$", ChinaComment);//中文注释
            str = str.Replace("$CurDate$", CommonHelper.GetCurDate());//当前时间
            str = str.Replace("$EntityName$", entityName);//实体类名

            str = str.Replace("$FilePrefixName$", filePrefixName);//模块名
            str = str.Replace("$Modulelogo$", Modulelogo);//模块简写
            str = str.Replace("$PrimaryKey$", primaryKey);//主键
            str = str.Replace("$PrimaryKeyDesc$", primaryKeyDesc);//描述
            str = str.Replace("$TableAlias$", tableAlias);//表别名

            str = str.Replace("$AddEntityParam$", addEntityParam);

            str = str.Replace("$ToSingleModel$", StructStrHelper.GetToModelStr(columnNameList));//动态给实体类赋值 
            return str;
        }
    }
}
