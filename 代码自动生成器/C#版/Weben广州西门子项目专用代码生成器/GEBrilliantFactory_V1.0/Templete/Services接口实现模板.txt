
using $ProjectNamePrefix$.Core.IServices;
using $ProjectNamePrefix$.Core.Model.Models;
using $ProjectNamePrefix$.Core.Services.BASE;
using $ProjectNamePrefix$.Core.IRepository.Base;

namespace $ProjectNamePrefix$.Core.Services
{
    /// <summary>
	/// $ChinaComment$服务接口实现
	/// </summary>	
    public class $EntityName$Services : BaseServices<$EntityName$>, I$EntityName$Services
    {
        private readonly IBaseRepository<$EntityName$> _dal;
        public $EntityName$Services(IBaseRepository<$EntityName$> dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }
    }
}